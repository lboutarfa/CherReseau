package com.solutec.CherReseau.interfaces;

public interface Alive {
    int getPV();
    void setPV(int pv);

    default void setDamages(int damage) {
        int remainingPV = this.getPV() - damage;
        this.setPV(Math.max(remainingPV, 0));
    }
    default boolean isAlive(){
        return this.getPV()>0;
    }

}
