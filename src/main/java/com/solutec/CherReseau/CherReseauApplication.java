package com.solutec.CherReseau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CherReseauApplication {

	public static void main(String[] args) {
		SpringApplication.run(CherReseauApplication.class, args);
	}

}
